package io.gitlab.restbase;

import static io.gitlab.restbase.Path.ROOT;
import static io.gitlab.restbase.PathUtils.path;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PathTest {

  @Test(expected = IllegalArgumentException.class)
  public void pathSegmentsCannotBeNull() {
    new Path((String) null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void pathSegmentsCannotBeEmpty() {
    new Path("");
  }

  @Test(expected = IllegalArgumentException.class)
  public void pathSegmentsCannotBeCombinedWithEmpty() {
    new Path("col", "");
  }

  @Test
  public void deserialize() throws Throwable {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();

    Path restored = objectMapper.readValue('"' + "a/b/c" + '"', Path.class);

    assertEquals(restored, path("a", "b", "c"));
  }

  @Test
  public void serialize() throws Throwable {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();

    Path path = path("a", "b", "c");
    String asString = objectMapper.writeValueAsString(path);
    assertNotNull(asString);
    assertEquals('"' + "a/b/c" + '"', asString);
  }

  @Test
  public void deserializeRoot() throws Throwable {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();

    Path restored = objectMapper.readValue('"' + "" + '"', Path.class);

    assertEquals(restored, Path.ROOT);
  }

  @Test
  public void serializeRoot() throws Throwable {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();

    Path path = Path.ROOT;
    String asString = objectMapper.writeValueAsString(path);
    assertNotNull(asString);
    assertEquals("\"\"", asString);
  }

  @Test
  public void testToString() {
    assertEquals("[]", ROOT.toString());
    assertEquals("[c1]", new Path("c1").toString());
    assertEquals("[c1, c2]", new Path("c1", "c2").toString());
  }

  @Test
  public void getSegments() {
    assertNotNull(ROOT.getSegments());
    assertEquals(0, ROOT.getSegments().size());
    
    assertNotNull(new Path("c1").getSegments());
    assertEquals(1, new Path("c1").getSegments().size());
    assertEquals("c1", new Path("c1").getSegments().get(0));
  }

  @Test
  public void isRoot() {
    assertTrue(ROOT.isRoot());
    assertFalse(new Path("c1").isRoot());
  }

  @Test
  public void rootHasEmptyCollectionName() {
    assertEquals(Optional.empty(), ROOT.collectionName());
  }

  @Test
  public void documentHasEmptyCollectionName() {
    assertEquals(Optional.empty(), new Path("c1", "c2").collectionName());
  }

  @Test
  public void regularCollectionName() {
    assertEquals(Optional.of("c1"), new Path("c1").collectionName());
    assertEquals(Optional.of("c3"), new Path("c1", "c2", "c3").collectionName());
  }

}
