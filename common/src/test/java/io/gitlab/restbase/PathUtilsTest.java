package io.gitlab.restbase;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.PathUtils.root;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;;

public class PathUtilsTest {

  @Test
  public void emptyPathIsRoot() {
    assertNotNull(path());
    assertEquals(root(), path());
  }

  @Test
  public void rootIsSame() {
    assertNotNull(root());
    assertEquals(root(), root());
  }

  @Test
  public void rootIsDocument() {
    assertTrue(root().isDocument());
    assertFalse(root().isCollection());
  }

  @Test
  public void firstIsCollection() {
    assertFalse(path("p1").isDocument());
    assertTrue(path("p1").isCollection());
  }

  @Test
  public void secondIsDocument() {
    assertTrue(path("p1", "2").isDocument());
    assertFalse(path("p1", "2").isCollection());
  }

  @Test
  public void thirdIsCollection() {
    assertFalse(path("p1", "2", "3").isDocument());
    assertTrue(path("p1", "2", "3").isCollection());
  }

  @Test
  public void simplePath() {
    assertNotNull(path("test"));
    assertEquals(path("test"), path("test"));
    assertEquals("test", path("test").joined());
  }

  @Test
  public void append() {
    assertEquals(root().append("test"), path("test"));
    assertEquals(path("test").append("2"), path("test", "2"));
  }

  @Test
  public void checkHash() {
    assertEquals(root().hashCode(), root().hashCode());
    assertEquals(path("test").hashCode(), path("test").hashCode());
    assertNotEquals(path("test1").hashCode(), path("test").hashCode());
  }

  @Test
  public void complexPath() {
    assertNotNull(path("test1", "test2"));
    assertEquals(path("test1", "test2"), path("test1", "test2"));
    assertEquals("test1/test2", path("test1", "test2").joined());
  }

  @Test
  public void rootDocumentKeyIsEmpty() {
    assertEquals(Optional.empty(), root().documentKey());
  }

  @Test
  public void collectionDocumentKeyIsEmpty() {
    assertEquals(Optional.empty(), path("test1").documentKey());
    assertEquals(Optional.empty(), path("test1", "test2", "test3").documentKey());
  }

  @Test
  public void regularDocumentKey() {
    assertEquals("test2", path("test1", "test2").documentKey()
      .get());
    assertEquals("test4", path("test1", "test2", "test3", "test4").documentKey()
      .get());
  }

  @Test
  public void rootParentIsRoot() {
    assertEquals(Path.ROOT, root().parent());
  }

  @Test
  public void firstLevelCollectionParentPathIsRoot() {
    assertEquals(Path.ROOT, path("test1").parent());
  }

  @Test
  public void secondLevelCollectionParentPathIsNotEmpty() {
    assertEquals(path("test1", "test2"), path("test1", "test2", "test3").parent());
  }

  @Test
  public void regularParentPath() {
    assertEquals(path("test1"), path("test1", "test2").parent());
    assertEquals(path("test1", "test2", "test3"), path("test1", "test2", "test3", "test4").parent());
  }

  @Test
  public void secondLevelCollectionParentPath() {
    assertEquals(path("col1", "doc1"), path("col1", "doc1", "test").parent());
  }

  @Test
  public void secondLevelCollectionName() {
    assertEquals("test", path("col1", "doc1", "test").collectionName().get());
  }

  @Test
  public void documentHasNoCollectionName() {
    assertEquals(Optional.empty(), path("col1", "doc1").collectionName());
  }
}
