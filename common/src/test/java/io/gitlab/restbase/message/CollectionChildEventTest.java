package io.gitlab.restbase.message;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.message.ChildEventType.ADDED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;

public class CollectionChildEventTest {

  ObjectMapper mapper = Jackson2ObjectMapperBuilder.json()
      .build();
  
  @Test(timeout = 5000)
  public void serialize() throws Exception {
    Path path = path("a", "b");
    DocumentWithId doc = new DocumentWithId(42L, path("a"));
    doc.set("field", "value");

    CollectionChildEvent value = new CollectionChildEvent(path, doc, ADDED);
    String s = mapper.writeValueAsString(value);
    CollectionChildEvent restored = mapper.readValue(s, CollectionChildEvent.class);
    
    assertNotNull(restored);
    assertEquals(path, restored.getParent());
    assertEquals(doc, restored.getDocument());
    assertEquals(ADDED, restored.getEventType());
    assertEquals(value, restored);
  }

}
