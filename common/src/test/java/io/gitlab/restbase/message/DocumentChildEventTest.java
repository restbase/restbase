package io.gitlab.restbase.message;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.message.ChildEventType.ADDED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.Path;

public class DocumentChildEventTest {

  ObjectMapper mapper = Jackson2ObjectMapperBuilder.json()
      .build();

  @Test(timeout = 5000)
  public void serialize() throws Exception {
    Path parent = path("a/b");
    String collection = "col1";
    DocumentChildEvent value = new DocumentChildEvent(parent, collection, ADDED);
    String s = mapper.writeValueAsString(value);
    DocumentChildEvent restored = mapper.readValue(s, DocumentChildEvent.class);

    assertNotNull(restored);
    assertEquals(parent, restored.getParent());
    assertEquals(collection, restored.getCollection());
    assertEquals(ADDED, restored.getEventType());
    assertEquals(value, restored);
  }

}
