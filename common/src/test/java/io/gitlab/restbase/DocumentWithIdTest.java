package io.gitlab.restbase;

import static io.gitlab.restbase.PathUtils.path;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

public class DocumentWithIdTest {

  @Test
  public void deserialize() throws IOException {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();
    DocumentWithId document = new DocumentWithId(42L, path("col"), ImmutableMap.of("string1", "test", "number1", 123));
    String asString = objectMapper.writeValueAsString(document);

    assertNotNull(asString);
    DocumentWithId restored = objectMapper.readValue(asString, DocumentWithId.class);
    assertEquals(restored, document);
    assertEquals("col", restored.getCollection());
    assertEquals(42L, (long) restored.getId());
    assertEquals(path("col/42"), restored.path());
    assertEquals(of("test"), restored.get("string1"));
    assertEquals(of(123), restored.get("number1"));
  }

  @Test
  public void deserializeRoot() throws IOException {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();
    DocumentWithId root = DocumentWithId.ROOT;
    String asString = objectMapper.writeValueAsString(root);

    assertNotNull(asString);
    DocumentWithId restored = objectMapper.readValue(asString, DocumentWithId.class);
    assertEquals(restored, root);
    assertEquals(null, restored.getCollection());
    assertEquals(Path.ROOT, restored.path());
  }

  @Test
  public void equals() {
    assertEquals(new DocumentWithId(42L, path("col")), new DocumentWithId(42L, path("col")));
    assertEquals(new DocumentWithId(42L, path("col"), ImmutableMap.of("string1", "test", "number1", 123)),
        new DocumentWithId(42L, path("col"), ImmutableMap.of("string1", "test", "number1", 123)));
  }

  @Test(expected = IllegalArgumentException.class)
  public void nullPathNotAllowed() {
    new DocumentWithId(42L, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void nullPathWithFieldsNotAllowed() {
    new DocumentWithId(42L, null, new HashMap<>());
  }

  @Test
  public void asString() {
    DocumentWithId document = new DocumentWithId(42L, path("col"));

    assertNotNull(document.toString());
  }

  @Test
  public void rootAsString() {
    DocumentWithId document = DocumentWithId.ROOT;

    assertNotNull(document.toString());
    assertTrue(document.toString()
      .contains("ROOT"));
  }

  @Test
  public void rootPathWithId() {
    DocumentWithId document = DocumentWithId.ROOT;
    document.setId(42L);

    assertNotNull(document.path());
    assertEquals(Path.ROOT, document.path());
  }

  @Test
  public void sameHashCodes() {
    DocumentWithId d1 = new DocumentWithId(42L, path("col"));
    DocumentWithId d2 = new DocumentWithId(42L, path("col"));

    assertEquals(d1.hashCode(), d2.hashCode());
  }

  @Test
  public void sameHashCodesEvenWhenFieldsDiffer() {
    DocumentWithId d1 = new DocumentWithId(42L, path("col"));
    DocumentWithId d2 = new DocumentWithId(42L, path("col"));
    d2.set("field", 123);

    assertEquals(d1.hashCode(), d2.hashCode());
  }

  @Test
  public void differentHashCodesWhenIdsDiffer() {
    DocumentWithId d1 = new DocumentWithId(1L, path("col"));
    DocumentWithId d2 = new DocumentWithId(2L, path("col"));

    assertNotEquals(d1.hashCode(), d2.hashCode());
  }

  @Test
  public void differentHashCodesWhenCollectionsDiffer() {
    DocumentWithId d1 = new DocumentWithId(42L, path("col1"));
    DocumentWithId d2 = new DocumentWithId(42L, path("col2"));

    assertNotEquals(d1.hashCode(), d2.hashCode());
  }

  @Test
  public void level1Parent() {
    DocumentWithId doc = new DocumentWithId(42L, path("col1"));
    assertNotNull(doc.getParent());
    assertEquals(path("col1"), doc.getParent());
  }

  @Test
  public void rootHasNoParent() {
    DocumentWithId doc = DocumentWithId.ROOT;
    assertNull(doc.getParent());
  }

  @Test
  public void rootEqualsRoot() {
    assertEquals(DocumentWithId.ROOT, DocumentWithId.ROOT);
  }

  @Test
  public void rootHasRootKey() {
    assertEquals(Document.ROOT_ID, DocumentWithId.ROOT.getId());
  }

}
