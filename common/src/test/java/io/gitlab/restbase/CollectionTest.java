package io.gitlab.restbase;

import static io.gitlab.restbase.PathUtils.path;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Path;

public class CollectionTest {

  @Test
  public void deserialize() throws IOException {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();
    Collection collection = new Collection(path("doc"), "col");
    String asString = objectMapper.writeValueAsString(collection);

    assertNotNull(asString);
    Collection restored = objectMapper.readValue(asString, Collection.class);
    assertEquals(restored, collection);
    assertEquals("doc", restored.getDocument());
    assertEquals("col", restored.getName());
    assertEquals(path("doc/col"), restored.path());
  }

  @Test
  public void deserializeRoot() throws IOException {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();
    Collection collection = new Collection(Path.ROOT, "col");
    String asString = objectMapper.writeValueAsString(collection);

    assertNotNull(asString);
    Collection restored = objectMapper.readValue(asString, Collection.class);
    assertEquals(restored, collection);
    assertEquals("", restored.getDocument());
    assertEquals("col", restored.getName());
    assertEquals(path("col"), restored.path());
  }

  @Test
  public void equals() {
    assertEquals(new Collection(path("doc"), "col"), new Collection(path("doc"), "col"));
    assertEquals(new Collection(Path.ROOT, "col"), new Collection(Path.ROOT, "col"));
  }

  @Test
  public void equalsWhenSame() {
    Collection col = new Collection(path("doc"), "col");
    assertEquals(col, col);
  }

  @Test(expected = IllegalArgumentException.class)
  public void nullPathNotAllowed() {
    new Collection(null, "col");
  }

  @Test
  public void asString() {
    Collection col = new Collection(path("doc"), "col");
    assertNotNull(col.toString());
  }

  @Test
  public void rootCollection() {
    Collection col = new Collection("col");
    assertEquals(Path.ROOT, col.getParent());
    assertEquals("col", col.getName());
  }

  @Test
  public void sameHashCodes() {
    Collection col1 = new Collection(path("doc"), "col");
    Collection col2 = new Collection(path("doc"), "col");

    assertEquals(col1.hashCode(), col2.hashCode());
  }

  @Test
  public void differentHashCodesWhenNamesDiffer() {
    Collection col1 = new Collection(path("doc"), "col1");
    Collection col2 = new Collection(path("doc"), "col2");

    assertNotEquals(col1.hashCode(), col2.hashCode());
  }

  @Test
  public void differentHashCodesWhenPathsDiffer() {
    Collection col1 = new Collection(path("doc1"), "col");
    Collection col2 = new Collection(path("doc2"), "col");

    assertNotEquals(col1.hashCode(), col2.hashCode());
  }

  @Test
  public void notEqualWhenNamesDiffer() {
    Collection col1 = new Collection(path("doc"), "col1");
    Collection col2 = new Collection(path("doc"), "col2");

    assertNotEquals(col1, col2);
  }

  @Test
  public void notEqualWhenPathsDiffer() {
    Collection col1 = new Collection(path("doc1"), "col");
    Collection col2 = new Collection(path("doc2"), "col");

    assertNotEquals(col1, col2);
  }

  @Test
  public void notEqualWhenNull() {
    Collection col = new Collection(path("doc1"), "col");

    assertNotEquals(col, null);
  }
}
