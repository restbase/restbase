package io.gitlab.restbase;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import io.gitlab.restbase.Document;

public class DocumentTest {

  @Test
  public void serialize() throws JsonProcessingException {
    ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
      .build();
    ImmutableMap<String, Object> map = ImmutableMap.of("test", "value");
    Document document = new Document(map);
    String documentString = objectMapper.writeValueAsString(document);

    assertNotNull(documentString);
    assertEquals(objectMapper.writeValueAsString(map), documentString);
  }

  @Test
  public void cloneAnother() {
    ImmutableMap<String, Object> map = ImmutableMap.of("test", "value");
    Document document = new Document(map);
    Document clone = new Document(document);

    assertNotNull(clone);
    assertEquals(document, clone);
  }

  @Test
  public void missingField() {
    ImmutableMap<String, Object> map = ImmutableMap.of("test", "value");
    Document document = new Document(map);

    assertEquals(Optional.empty(), document.get("missing"));
  }

  @Test
  public void sameHashCodes() {
    ImmutableMap<String, Object> map = ImmutableMap.of("test", "value");
    Document d1 = new Document(map);
    Document d2 = new Document(map);

    assertEquals(d1.hashCode(), d2.hashCode());
  }

  @Test
  public void differentHashCodes() {
    ImmutableMap<String, Object> map = ImmutableMap.of("test", "value");
    Document d1 = new Document(map);
    Document d2 = new Document(map);
    d2.set("field", 123);

    assertNotEquals(d1.hashCode(), d2.hashCode());
  }

  @Test
  public void string() {
    ImmutableMap<String, Object> map = ImmutableMap.of("test", "value");
    Document document = new Document(map);

    assertNotNull(document.toString());
  }
}
