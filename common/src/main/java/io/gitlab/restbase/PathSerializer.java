package io.gitlab.restbase;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

class PathSerializer extends JsonSerializer<Path>{

  @Override
  public void serialize(Path value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
    gen.writeString(value.joined());
  }

}
