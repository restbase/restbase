package io.gitlab.restbase;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.relational.core.mapping.Column;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Document implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final Long ROOT_ID = -1L;

  @Column
  protected Map<String, Object> fields;

  @JsonIgnore
  protected String collection;

  public Document() {
    this.fields = new HashMap<>();
  }

  public Document(Map<String, Object> fields) {
    this.fields = new HashMap<>(fields);
  }

  public Document(Document other) {
    this.fields = new HashMap<>(other.fields);
    this.collection = other.collection;
  }

  @JsonAnySetter
  public <V> Document set(String field, V value) {
    fields.put(field, value);
    return this;
  }

  @JsonAnyGetter
  public Map<String, Object> getFields() {
    return fields;
  }

  @SuppressWarnings("unchecked")
  public <V> Optional<V> get(String field) {
    Object value = fields.get(field);
    if (value == null)
      return Optional.empty();
    return (Optional<V>) Optional.of(value);
  }

  public void setCollection(String parentPath) {
    this.collection = parentPath;
  }

  public String getCollection() {
    return collection;
  }

  @Override
  public int hashCode() {
    return Objects.hash(fields);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Document other = (Document) obj;
    return Objects.equals(fields, other.fields);
  }

  @Override
  public String toString() {
    return MessageFormat.format("Document @{0}, {1}", collection, fields);
  }

}
