package io.gitlab.restbase;

import static org.springframework.util.StringUtils.isEmpty;

import java.util.Map;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Table("document")
public class DocumentWithId extends Document {

  public static DocumentWithId ROOT = new DocumentWithId();
  private static final long serialVersionUID = 1L;

  @Id
  private Long id;

  private DocumentWithId() {
    id = ROOT_ID;
  }

  public DocumentWithId(Long id, Path parent, Map<String, Object> fields) {
    super(fields);
    if (parent == null)
      throw new IllegalArgumentException("Parent path cannot be null");
    this.id = id;
    this.collection = parent.joined();
  }

  public DocumentWithId(Long id, Path parent) {
    if (parent == null)
      throw new IllegalArgumentException("Parent path cannot be null");
    this.id = id;
    this.collection = parent.joined();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @JsonIgnore
  public Path path() {
    if (isRoot())
      return Path.ROOT;
    return getParent().append(id);
  }

  @JsonGetter
  @Override
  public String getCollection() {
    return super.getCollection();
  }

  @JsonIgnore
  public Path getParent() {
    if (isRoot())
      return null;
    return PathUtils.path(collection);
  }

  private boolean isRoot() {
    return isEmpty(id) || isEmpty(collection);
  }

  @Override
  public String toString() {
    if (isRoot())
      return String.format("DocumentWithId [ROOT] = %s", getFields());
    return String.format("DocumentWithId [%s@%s] = %s", id, collection, getFields());
  }

  @Override
  public int hashCode() {
    return Objects.hash(collection, id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DocumentWithId other = (DocumentWithId) obj;
    if (isRoot() && other.isRoot()) {
      return true;
    }
    return Objects.equals(collection, other.collection) && Objects.equals(id, other.id) && super.equals(other);
  }

}
