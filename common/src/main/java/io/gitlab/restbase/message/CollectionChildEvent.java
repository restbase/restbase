package io.gitlab.restbase.message;

import java.io.Serializable;
import java.util.Objects;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;

public class CollectionChildEvent implements Serializable, ChildEvent {

  private static final long serialVersionUID = 1L;

  private Path parent;
  private DocumentWithId document;
  private ChildEventType eventType;

  public CollectionChildEvent() {
  }

  public CollectionChildEvent(Path parent, DocumentWithId document, ChildEventType eventType) {
    this.parent = parent;
    this.document = document;
    this.eventType = eventType;
  }

  @Override
  public Path getParent() {
    return parent;
  }

  public DocumentWithId getDocument() {
    return document;
  }

  @Override
  public ChildEventType getEventType() {
    return eventType;
  }

  @Override
  public String toString() {
    return String.format("CollectionChildEvent [parent=%s, document=%s, eventType=%s]", parent, document, eventType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(document, parent, eventType);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CollectionChildEvent other = (CollectionChildEvent) obj;
    return Objects.equals(parent, other.parent) && Objects.equals(eventType, other.eventType)
        && Objects.equals(document, other.document);
  }

}
