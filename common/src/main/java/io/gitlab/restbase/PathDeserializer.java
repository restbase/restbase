package io.gitlab.restbase;

import static io.gitlab.restbase.PathUtils.isRoot;
import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.PathUtils.root;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

class PathDeserializer extends JsonDeserializer<Path> {

  @Override
  public Path deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    String string = p.getCodec()
      .readValue(p, String.class);
    if (isRoot(string))
      return root();
    return path(string);
  }

}
