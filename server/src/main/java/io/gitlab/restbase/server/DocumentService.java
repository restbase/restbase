package io.gitlab.restbase.server;

import static io.gitlab.restbase.Document.ROOT_ID;
import static io.gitlab.restbase.server.util.FluxUtils.deferError;
import static io.gitlab.restbase.server.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.server.exception.NotFoundException;
import io.gitlab.restbase.server.message.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class DocumentService {

  private final DocumentRepository documents;
  private final Publisher publisher;
  public static final String DOCUMENT_ROOT_ID = "" + ROOT_ID;

  public DocumentService(DocumentRepository documents, Publisher publisher) {
    this.documents = documents;
    this.publisher = publisher;
  }

  public Mono<DocumentWithId> get(Path path) {
    Mono<DocumentWithId> documentRef = Mono.justOrEmpty(path.documentKey())
      .map(documentKey -> this.parseDocumentId(documentKey))
      .flatMap((documentId -> documents.findById(documentId)));
    return wrapNotFoundIfEmpty(documentRef, "document", path.toString());
  }

  private Long parseDocumentId(String key) {
    long result;
    try {
      result = Long.parseLong(key);
    } catch (NumberFormatException e) {
      result = ROOT_ID;
    }
    return result;
  }

  public Mono<DocumentWithId> getRoot() {
    return documents.findById(ROOT_ID)
      .switchIfEmpty(Mono.defer(() -> {
        DocumentWithId root = DocumentWithId.ROOT;
        return documents.save(root);
      }));
  }

  public Mono<DocumentWithId> delete(Path path) {
    Mono<DocumentWithId> documentRef = Mono.justOrEmpty(path.documentKey())
      .map(documentKey -> this.parseDocumentId(documentKey))
      .flatMap((documentId -> documents.findById(documentId)));
    return wrapNotFoundIfEmpty(documentRef.flatMap(doc -> documents.deleteById(doc.getId())
      .thenReturn(doc)), "document", path.toString()).doOnNext(doc -> publisher.publishDocumentDeleted(doc));
  }

  public Mono<DocumentWithId> save(Path path, Document document) {
    if (path.isCollection()) {
      DocumentWithId doc = new DocumentWithId(null, path, document.getFields());
      return documents.save(doc)
        .doOnNext(d -> publisher.publishDocumentAdded(d));
    }
    return deferError(new NotFoundException("collection", path.toString()));
  }

  public Flux<DocumentWithId> getAll(Path path) {
    Flux<DocumentWithId> document = documents.findByCollection(path.joined());
    return wrapNotFoundIfEmpty(document, "document", path.toString());
  }

  public Mono<DocumentWithId> update(Path path, @Valid DocumentWithId document) {
    if (path.isDocument()) {
      Path parent = path.parent();
      DocumentWithId doc = new DocumentWithId(document.getId(), parent, document.getFields());
      return documents.save(doc)
        .doOnNext(d -> publisher.publishDocumentUpdated(d));
    }
    return deferError(new NotFoundException("document", path.toString()));
  }

}
