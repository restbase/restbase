package io.gitlab.restbase.server.message;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class MessageConfiguration {

  @Bean
  public ConnectionFactory rabbitConnectionFactory(RabbitProperties rabbit) {
    CachingConnectionFactory connectionFactory = rabbit.getPort() > 0
        ? new CachingConnectionFactory(rabbit.getHost(), rabbit.getPort())
        : new CachingConnectionFactory(rabbit.getHost());
    connectionFactory.setUsername(rabbit.getUsername());
    connectionFactory.setPassword(rabbit.getPassword());
    return connectionFactory;
  }

  @Bean
  public RabbitAdmin admin(ConnectionFactory connectionFactory) {
    return new RabbitAdmin(connectionFactory);
  }

  @Bean
  public MessageConverter messageConverter(ObjectMapper objectMapper) {
    return new Jackson2JsonMessageConverter(objectMapper);
  }
}
