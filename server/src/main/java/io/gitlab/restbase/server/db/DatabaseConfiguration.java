package io.gitlab.restbase.server.db;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.connectionfactory.init.CompositeDatabasePopulator;
import org.springframework.data.r2dbc.connectionfactory.init.ConnectionFactoryInitializer;
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator;
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.server.CollectionRepository;
import io.gitlab.restbase.server.DocumentRepository;
import io.r2dbc.spi.ConnectionFactory;

@Configuration
@EnableR2dbcRepositories(basePackageClasses = { CollectionRepository.class, DocumentRepository.class })
public class DatabaseConfiguration extends AbstractR2dbcConfiguration {

  private final ConnectionFactory connectionFactory;
  private final ObjectMapper objectMapper;
  private final DatabaseMigrationProperties properties;
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  public DatabaseConfiguration(ConnectionFactory connectionFactory, ObjectMapper objectMapper,
      DatabaseMigrationProperties properties) {
    this.connectionFactory = connectionFactory;
    this.objectMapper = objectMapper;
    this.properties = properties;
  }

  @Bean
  public ConnectionFactoryInitializer initializer() {

    ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
    initializer.setConnectionFactory(connectionFactory);

    CompositeDatabasePopulator populator = new CompositeDatabasePopulator();
    if (properties.hasInitScript()) {
      String initScript = properties.getInitScript();
      logger.info("DB init script'{}'", initScript);
      populator.addPopulators(new ResourceDatabasePopulator(new ClassPathResource(initScript)));
    }
    if (properties.hasCleanScript()) {
      String cleanScript = properties.getCleanScript();
      logger.info("DB clean script'{}'", cleanScript);
      populator.addPopulators(new ResourceDatabasePopulator(new ClassPathResource(cleanScript)));
    }
    initializer.setDatabasePopulator(populator);

    return initializer;
  }

  @Bean
  @Override
  public R2dbcCustomConversions r2dbcCustomConversions() {
    List<Converter<?, ?>> converters = new ArrayList<>();
    converters.add(new JsonToHashMapConverter(objectMapper));
    converters.add(new JsonToMapConverter(objectMapper));
    converters.add(new HashMapToJsonConverter(objectMapper));
    converters.add(new MapToJsonConverter(objectMapper));
    return new R2dbcCustomConversions(getStoreConversions(), converters);
  }

  @Override
  public ConnectionFactory connectionFactory() {
    return connectionFactory;
  }
}
