package io.gitlab.restbase.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import io.gitlab.restbase.server.db.DatabaseMigrationProperties;

@SpringBootApplication
@Configuration
@EnableConfigurationProperties(DatabaseMigrationProperties.class)
public class RestbaseServer {

  public static void main(String[] args) {
    SpringApplication.run(RestbaseServer.class, args);
  }

}
