package io.gitlab.restbase.server;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.lang.NonNull;

import io.gitlab.restbase.DocumentWithId;
import reactor.core.publisher.Flux;

public interface DocumentRepository extends ReactiveCrudRepository<DocumentWithId, Long> {

  @Query("SELECT * FROM document WHERE collection = :collection")
  Flux<DocumentWithId> findByCollection(@NonNull String collection);

}
