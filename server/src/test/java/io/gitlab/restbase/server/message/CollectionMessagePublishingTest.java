package io.gitlab.restbase.server.message;

import static io.gitlab.restbase.Path.ROOT;
import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.server.DocumentService.DOCUMENT_ROOT_ID;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.server.CollectionRepository;
import io.gitlab.restbase.server.CollectionService;
import reactor.core.publisher.Mono;

public class CollectionMessagePublishingTest {

  CollectionService service;

  CollectionRepository repository;

  Publisher publisher;

  @Before
  public void setUp() {
    repository = mock(CollectionRepository.class);
    publisher = mock(Publisher.class);
    service = new CollectionService(repository, publisher);
  }

  @Test(timeout = 5000)
  public void createCollectionAtRoot() throws Exception {
    Collection collection = new Collection();
    collection.setName("test");

    when(repository.save(collection)).thenReturn(Mono.just(collection));
    Path path = ROOT;
    service.save(path, collection)
      .block();

    verify(publisher, times(1)).publishCollectionAdded(collection);
  }

  @Test(timeout = 5000)
  public void createCollectionAtLevel3() throws Exception {
    Collection collection = new Collection();
    collection.setName("test");

    when(repository.save(collection)).thenReturn(Mono.just(collection));
    Path path = path("a", "b");
    service.save(path, collection)
      .block();

    verify(publisher, times(1)).publishCollectionAdded(collection);
  }

  @Test(timeout = 5000)
  public void deleteCollectionAtRoot() throws Exception {
    Collection collection = new Collection();
    collection.setName("test");
    collection.setDocument(DOCUMENT_ROOT_ID);
    when(repository.findByDocumentAndName(DOCUMENT_ROOT_ID, "test")).thenReturn(Mono.just(collection));
    when(repository.deleteByDocumentAndName(DOCUMENT_ROOT_ID, "test")).thenReturn(Mono.empty());

    service.deleteAtRoot(collection.getName())
      .block();

    verify(publisher, times(1)).publishCollectionDeleted(DOCUMENT_ROOT_ID, collection.getName());
  }

  @Test(timeout = 5000)
  public void deleteCollectionAtLevel3() throws Exception {
    Collection collection = new Collection();
    collection.setName("test");
    collection.setDocument("a/b");

    when(repository.findByDocumentAndName("a/b", "test")).thenReturn(Mono.just(collection));
    when(repository.deleteByDocumentAndName("a/b", "test")).thenReturn(Mono.empty());
    Path path = path("a", "b", collection.getName());
    service.delete(path)
      .block();

    verify(publisher, times(1)).publishCollectionDeleted("a/b", collection.getName());
  }

}
