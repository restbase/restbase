package io.gitlab.restbase.server;

import static io.gitlab.restbase.PathUtils.path;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = NONE)

public class DocumentFieldsTest {

  @Autowired
  private DocumentRepository repository;

  @Before
  public void clean() {
    repository.deleteById(repository.findAll()
      .map(doc -> doc.getId())
      .filter(id -> id != Document.ROOT_ID))
      .block();
  }

  @Test
  public void emptyDocument() {
    Mono<DocumentWithId> saveAndGet = repository.save(new DocumentWithId(null, path("path")))
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertFalse(d.get("missing")
          .isPresent());
      })
      .verifyComplete();
  }

  @Test
  public void stringField() {
    DocumentWithId documentWithId = new DocumentWithId(null, path("path"));
    documentWithId.set("string1", "test");
    Mono<DocumentWithId> saveAndGet = repository.save(documentWithId)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertTrue(d.get("string1")
          .isPresent());
        assertEquals("test", d.get("string1")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void integerField() {
    DocumentWithId documentWithId = new DocumentWithId(null, path("path"));
    documentWithId.set("integer1", 123);
    Mono<DocumentWithId> saveAndGet = repository.save(documentWithId)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertTrue(d.get("integer1")
          .isPresent());
        assertEquals(123, d.get("integer1")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void doubleField() {
    DocumentWithId documentWithId = new DocumentWithId(null, path("path"));
    documentWithId.set("double1", 123.123d);
    Mono<DocumentWithId> saveAndGet = repository.save(documentWithId)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertTrue(d.get("double1")
          .isPresent());
        assertEquals(123.123d, d.get("double1")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void booleanField() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("boolean1", true);
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertEquals(true, d.get("boolean1")
          .get());
        assertEquals(Boolean.TRUE, d.get("boolean1")
          .get());
        assertEquals(false, d.get("boolean2")
          .isPresent());
      })
      .verifyComplete();
  }

  @Test
  public void stringArrayField() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("array", new String[] { "a", "b" });
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertNotNull(d.get("array"));
        assertTrue(d.get("array")
          .isPresent());
        assertEquals(Arrays.asList("a", "b"), d.get("array")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void integerArrayField() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("array", new int[] { 1, 2 });
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertNotNull(d.get("array"));
        assertTrue(d.get("array")
          .isPresent());
        assertEquals(Arrays.asList(1, 2), d.get("array")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void doubleArrayField() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("array", new double[] { 1.123, 2.123d });
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertNotNull(d.get("array"));
        assertTrue(d.get("array")
          .isPresent());
        assertEquals(Arrays.asList(1.123, 2.123d), d.get("array")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void booleanArrayField() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("array", new boolean[] { true, false });
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertNotNull(d.get("array"));
        assertTrue(d.get("array")
          .isPresent());
        assertEquals(Arrays.asList(true, false), d.get("array")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void complex() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("string", "test")
      .set("integer1", 123)
      .set("double1", 123.123d)
      .set("boolean1", true)
      .set("stringArray", new String[] { "a", "b" })
      .set("intArray", new int[] { 1, 2 })
      .set("doubleArray", new double[] { 1.123, 2.123d })
      .set("booleanArray", new boolean[] { true, false });
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertNotNull(d.get("string"));
        assertTrue(d.get("string")
          .isPresent());
        assertTrue(d.get("integer1")
          .isPresent());
        assertEquals(123, d.get("integer1")
          .get());
        assertTrue(d.get("double1")
          .isPresent());
        assertEquals(123.123d, d.get("double1")
          .get());
        assertEquals(true, d.get("boolean1")
          .get());
        assertEquals(Boolean.TRUE, d.get("boolean1")
          .get());
        assertEquals(false, d.get("boolean2")
          .isPresent());
        assertNotNull(d.get("stringArray"));
        assertTrue(d.get("stringArray")
          .isPresent());
        assertEquals(Arrays.asList("a", "b"), d.get("stringArray")
          .get());
        assertNotNull(d.get("intArray"));
        assertTrue(d.get("intArray")
          .isPresent());
        assertEquals(Arrays.asList(1, 2), d.get("intArray")
          .get());
        assertNotNull(d.get("doubleArray"));
        assertTrue(d.get("doubleArray")
          .isPresent());
        assertEquals(Arrays.asList(1.123, 2.123d), d.get("doubleArray")
          .get());
        assertNotNull(d.get("booleanArray"));
        assertTrue(d.get("booleanArray")
          .isPresent());
        assertEquals(Arrays.asList(true, false), d.get("booleanArray")
          .get());
      })
      .verifyComplete();
  }

}
