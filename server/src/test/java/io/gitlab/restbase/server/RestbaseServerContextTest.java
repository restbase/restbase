package io.gitlab.restbase.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class RestbaseServerContextTest {

  @Test
  public void contextLoads() {
    RestbaseServer.main(new String[] {});
  }

}
