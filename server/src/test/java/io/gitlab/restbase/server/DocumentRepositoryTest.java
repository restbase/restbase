package io.gitlab.restbase.server;

import static io.gitlab.restbase.PathUtils.path;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = NONE)

public class DocumentRepositoryTest {

  @Autowired
  private DocumentRepository repository;

  @Before
  public void clean() {
    repository.deleteById(repository.findAll()
      .map(doc -> doc.getId())
      .filter(id -> id != Document.ROOT_ID))
      .block();
  }

  @Test
  public void saveAndCount() {
    List<DocumentWithId> listToSave = Arrays.asList(new DocumentWithId(null, path("path")),
        new DocumentWithId(null, path("path")));

    Long initialCount = repository.count()
      .block();

    Mono<Long> saveAndCount = repository.saveAll(listToSave)
      .last()
      .flatMap(v -> repository.count());

    StepVerifier.create(saveAndCount)
      .expectNext(initialCount + 2)
      .verifyComplete();
  }

  @Test
  public void saveAndLoad() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("field", "value");
    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
        assertTrue(d.get("field")
          .isPresent());
        assertEquals("value", d.get("field")
          .get());
      })
      .verifyComplete();
  }

  @Test
  public void saveAndDelete() {
    DocumentWithId document = new DocumentWithId(null, path("path"));
    document.set("field", "value");

    Mono<DocumentWithId> saveAndGet = repository.save(document)
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));
    Long id = saveAndGet.block()
      .getId();

    repository.deleteById(id)
      .block();

    Mono<DocumentWithId> findById = repository.findById(id);
    // check deleted does not exist
    StepVerifier.create(findById)
      .verifyComplete();
  }

  @Test
  public void saveDocumentWithoutCollectionFails() {
    Mono<DocumentWithId> saveAndGet = repository.save(new DocumentWithId(null, path("path")))
      .map(d -> d.getId())
      .flatMap(id -> repository.findById(id));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getId());
      })
      .verifyComplete();
  }
}
