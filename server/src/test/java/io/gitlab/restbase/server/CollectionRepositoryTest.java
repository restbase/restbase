package io.gitlab.restbase.server;

import static io.gitlab.restbase.PathUtils.path;
import static io.gitlab.restbase.server.DocumentService.DOCUMENT_ROOT_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.Counter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = NONE)

public class CollectionRepositoryTest {

  @Autowired
  private CollectionRepository repository;

  @Before
  public void cleanUp() {
    repository.deleteAll()
      .block();
  }

  @Test
  public void saveAndCount() {
    Collection[] collections = new Collection[] { new Collection(path("test-path"), "1"),
        new Collection(path("test-path"), "2") };
    Mono<Long> saveAndCount = repository.count()
      .thenMany(repository.saveAll(Flux.just(collections)))
      .last()
      .flatMap(v -> repository.count());

    StepVerifier.create(saveAndCount)
      .expectNext(Long.valueOf(collections.length))
      .verifyComplete();
  }

  @Test
  public void saveAndLoad() {
    Collection collection = new Collection("test-name");
    collection.setDocument("test-path");
    Mono<Collection> saveAndGet = repository.save(collection)
      .map(Collection::getDocument)
      .flatMap(d -> repository.findByDocumentAndName(d, "test-name"));

    StepVerifier.create(saveAndGet)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getName());
      })
      .verifyComplete();
  }

  @Test
  public void namesAreUnique() {
    Collection col1 = new Collection("test");
    Collection col2 = new Collection("test");
    Mono<Collection> saveAndGet1 = repository.save(col1);
    Mono<Collection> saveAndGet2 = repository.save(col2);

    StepVerifier.create(saveAndGet1)
      .assertNext(d -> {
        assertNotNull(d);
        assertNotNull(d.getName());
      })
      .verifyComplete();

    StepVerifier.create(saveAndGet2)
      .expectError();
  }

  @Test
  public void saveAndCheckExists() {
    String name = "test-name";
    String document = "test-path";

    Long initialCount = repository.countByDocumentAndName(document, name)
      .block();

    Collection collection = new Collection(name);
    collection.setDocument(document);
    Mono<Long> saveAndCheck = repository.save(collection)
      .map(Collection::getDocument)
      .flatMap(d -> repository.countByDocumentAndName(d, "test-name"));

    StepVerifier.create(saveAndCheck)
      .assertNext(d -> {
        assertNotNull(d);
        assertEquals(initialCount + 1, (long) d);
      })
      .verifyComplete();
  }

  @Test
  public void saveAndDelete() {
    String name = "test-name";
    String document = "test-path";

    Collection collection = new Collection(name);
    collection.setDocument(document);
    Mono<Collection> saveAndGet = repository.save(collection)
      .map(Collection::getDocument)
      .flatMap(d -> repository.findByDocumentAndName(d, name));
    Collection col = saveAndGet.block();

    assertNotNull(col);
    assertEquals(name, col.getName());
    assertEquals(document, col.getDocument());

    repository.deleteById(col.getId())
      .block();

    Mono<Collection> findById = repository.findById(col.getId());
    // check deleted does not exist
    StepVerifier.create(findById)
      .verifyComplete();
  }

  @Test
  public void collectionAtRoot() {
    Collection collection = new Collection();
    collection.setDocument(DOCUMENT_ROOT_ID);
    collection.setName("test");
    Mono<Collection> saved = repository.save(collection);

    StepVerifier.create(saved)
      .assertNext(d -> {
        assertNotNull(d);
      })
      .verifyComplete();

    Flux<Collection> all = repository.findByDocument(DOCUMENT_ROOT_ID);
    Counter counter = Counter.startWith(0);
    StepVerifier.create(all)
      .assertNext(d -> {
        assertNotNull(d);
        counter.inc();
      })
      .verifyComplete();

    assertEquals(1, counter.value());
  }

  @Test
  public void collectionMustHaveAPath() {
    Collection collection = new Collection();
    collection.setName("test");
    collection.setDocument(null);
    Mono<Collection> saveAndCheck = repository.save(collection);

    StepVerifier.create(saveAndCheck)
      .expectError();
  }

  @Test
  public void collectionMustHaveAName() {
    Collection collection = new Collection();
    collection.setName(null);
    collection.setDocument("test");
    Mono<Collection> saveAndCheck = repository.save(collection);

    StepVerifier.create(saveAndCheck)
      .expectError();
  }
}
