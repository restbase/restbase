package io.gitlab.restbase.server.message;

import static io.gitlab.restbase.server.message.Publisher.STOMP_TOPIC_EXCHANGE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import io.gitlab.restbase.message.ChildEventType;
import io.gitlab.restbase.message.CollectionChildEvent;
import io.gitlab.restbase.message.DocumentChildEvent;

@Component
public class PublisherTestListener {

  final Logger logger = LoggerFactory.getLogger(getClass());

  private CollectionChildEvent documentAdded;
  private CollectionChildEvent documentDeleted;
  private DocumentChildEvent collectionAdded;
  private DocumentChildEvent collectionDeleted;

  @RabbitListener(bindings = @QueueBinding(value = @Queue(), key = "a.children", exchange = @Exchange(value = STOMP_TOPIC_EXCHANGE, type = ExchangeTypes.TOPIC)))
  public void onDocumentAdded(@Payload CollectionChildEvent event) {
    logger.info("onDocumentAdded: {}", event);
    if (event.getEventType() == ChildEventType.ADDED) {
      this.documentAdded = event;
    }
  }

  @RabbitListener(bindings = @QueueBinding(value = @Queue(), key = "a.children", exchange = @Exchange(value = STOMP_TOPIC_EXCHANGE, type = ExchangeTypes.TOPIC)))
  public void onDocumentDeleted(@Payload CollectionChildEvent event) {
    logger.info("onDocumentDeleted: {}", event);
    if (event.getEventType() == ChildEventType.REMOVED) {
      this.documentDeleted = event;
    }
  }

  @RabbitListener(bindings = @QueueBinding(value = @Queue(), key = "a@@@@b.children", exchange = @Exchange(value = STOMP_TOPIC_EXCHANGE, type = ExchangeTypes.TOPIC)))
  public void onCollectionAdded(@Payload DocumentChildEvent event) {
    logger.info("onCollectionAdded: {}", event);
    if (event.getEventType() == ChildEventType.ADDED) {
      this.collectionAdded = event;
    }
  }

  @RabbitListener(bindings = @QueueBinding(value = @Queue(), key = "a@@@@b.children", exchange = @Exchange(value = STOMP_TOPIC_EXCHANGE, type = ExchangeTypes.TOPIC)))
  public void onCollectionDeleted(@Payload DocumentChildEvent event) {
    logger.info("onCollectionDeleted: {}", event);
    if (event.getEventType() == ChildEventType.REMOVED) {
      this.collectionDeleted = event;
    }
  }

  public CollectionChildEvent getDocumentAdded() {
    return documentAdded;
  }

  public CollectionChildEvent getDocumentDeleted() {
    return documentDeleted;
  }

  public DocumentChildEvent getCollectionAdded() {
    return collectionAdded;
  }

  public DocumentChildEvent getCollectionDeleted() {
    return collectionDeleted;
  }

}
