package io.gitlab.restbase.server.message;

import static io.gitlab.restbase.PathUtils.path;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.message.CollectionChildEvent;
import io.gitlab.restbase.message.DocumentChildEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = NONE)

public class PublisherTest {

  final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  Publisher publisher;

  @Autowired
  PublisherTestListener listener;

  @Test(timeout = 5000)
  public void testPublishDocumentAdded() throws Throwable {
    DocumentWithId doc = new DocumentWithId(42L, path("a"));
    doc.set("field", "value");

    logger.debug("TestListener: {}", listener);
    logger.info("publishDocumentUpdated: {}", doc);
    publisher.publishDocumentAdded(doc);

    sleep(100);
    CollectionChildEvent event = listener.getDocumentAdded();
    logger.info("event: {}", event);
    assertNotNull(event);
    assertEquals(path("a"), event.getParent());
    assertEquals(doc, event.getDocument());
  }

  @Test(timeout = 3000)
  public void testPublishDocumentDeleted() throws Throwable {
    DocumentWithId doc = new DocumentWithId(42L, path("a"));
    doc.set("field", "value");
    logger.info("publishDocumentDeleted: {}", doc);
    publisher.publishDocumentDeleted(doc);

    sleep(100);
    CollectionChildEvent event = listener.getDocumentDeleted();
    logger.info("event: {}", event);
    assertNotNull(event);
    assertEquals(path("a"), event.getParent());
    assertEquals(doc, event.getDocument());
  }

  @Test(timeout = 3000)
  public void testPublishCollectionAdded() throws Throwable {
    Collection collection = new Collection("c");
    collection.setDocument("a/b");
    logger.info("publishCollectionAdded: {}", collection);
    publisher.publishCollectionAdded(collection);

    sleep(100);
    DocumentChildEvent event = listener.getCollectionAdded();
    logger.info("event: {}", event);
    assertNotNull(event);
    assertEquals(path("a/b"), event.getParent());
    assertEquals(collection.getName(), event.getCollection());
  }

  @Test(timeout = 3000)
  public void testPublishCollectionDeleted() throws Throwable {
    Collection collection = new Collection("c");
    collection.setDocument("a/b");
    logger.info("publishCollectionDeleted: {}", collection);
    publisher.publishCollectionDeleted(collection.getDocument(), collection.getName());

    sleep(100);
    DocumentChildEvent event = listener.getCollectionDeleted();
    logger.info("event: {}", event);
    assertNotNull(event);
    assertEquals(path("a/b"), event.getParent());
    assertEquals(collection.getName(), event.getCollection());
  }

}
