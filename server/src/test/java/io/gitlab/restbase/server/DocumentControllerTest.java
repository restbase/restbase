package io.gitlab.restbase.server;

import static io.gitlab.restbase.PathUtils.path;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import io.gitlab.restbase.Document;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;

@RunWith(SpringRunner.class)
@SpringBootTest

public class DocumentControllerTest {

  @Autowired
  private ApplicationContext context;

  @Autowired
  private CollectionRepository collections;

  @Autowired
  private DocumentRepository documents;

  WebTestClient rest;

  @Before
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
      .build();
  }

  @After
  public void cleanUp() {
    collections.deleteAll()
      .block();
    documents.deleteById(documents.findAll()
      .map(doc -> doc.getId())
      .filter(id -> id != Document.ROOT_ID))
      .block();
  }

  @Test
  public void getRoot() throws Exception {
    DocumentWithId root = rest.get()
      .uri("/api")
      .accept(APPLICATION_JSON)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(root);
    assertNotNull(root.getId());
  }

  @Test
  public void createAndGetDocumentAtLevel1() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/test1")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("test1", inserted.getCollection());
    assertTrue(inserted.get("f1")
      .isPresent());
    assertEquals("val1", inserted.get("f1")
      .get());

    rest.get()
      .uri("/api/test1/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetDocumentAtLevel2() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/doc1/col2")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("col1/doc1/col2", inserted.getCollection());
    assertTrue(inserted.get("f1")
      .isPresent());
    assertEquals("val1", inserted.get("f1")
      .get());

    rest.get()
      .uri("/api/col1/doc1/col2/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetDocumentAtLevel3() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("col1/doc1/col2/doc2/col3", inserted.getCollection());
    assertTrue(inserted.get("f1")
      .isPresent());
    assertEquals("val1", inserted.get("f1")
      .get());

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndGetDocumentAtLevel4() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(inserted);
    assertNotNull(inserted.getId());
    assertEquals("col1/doc1/col2/doc2/col3/doc3/col4", inserted.getCollection());
    assertTrue(inserted.get("f1")
      .isPresent());
    assertEquals("val1", inserted.get("f1")
      .get());

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);
  }

  @Test
  public void createAndDeleteDocumentAtLevel1() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);

    rest.get()
      .uri("/api/col1/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteDocumentAtLevel2() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/doc1/col2")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/col2/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);

    rest.get()
      .uri("/api/col1/doc1/col2/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteDocumentAtLevel3() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/col2/doc2/col3/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void createAndDeleteDocumentAtLevel4() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    rest.delete()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .isEqualTo(inserted);

    rest.get()
      .uri("/api/col1/doc1/col2/doc2/col3/doc3/col4/{id}", inserted.getId())
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void getMissingDocument() throws Exception {
    rest.get()
      .uri("/api/{id}", "missing")
      .exchange()
      .expectStatus()
      .isNotFound();
  }

  @Test
  public void updateRootDocument() throws Exception {
    DocumentWithId root = DocumentWithId.ROOT;
    root.set("newField", "newValue");

    DocumentWithId updated = rest.put()
      .uri("/api")
      .accept(APPLICATION_JSON)
      .bodyValue(root)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(updated);
    assertEquals(Path.ROOT.joined(), updated.path()
      .joined());
    assertEquals(Optional.of("newValue"), updated.get("newField"));
  }

  @Test
  public void createAndUpdateDocumentAtLevel1() throws Exception {
    Document doc = new Document();
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    inserted.set("newField", "newValue");
    DocumentWithId updated = rest.put()
      .uri("/api/col1/{id}", inserted.getId())
      .accept(APPLICATION_JSON)
      .bodyValue(inserted)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(updated);
    assertEquals(path("col1/" + inserted.getId()), updated.path());
    assertEquals(Optional.of("newValue"), updated.get("newField"));
  }

  @Test
  public void createAndUpdateDocumentAtLevel2() throws Exception {
    Document doc = new Document();
    doc.setCollection("col1/111/col2");
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/111/col2")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    inserted.set("newField", "newValue");
    DocumentWithId updated = rest.put()
      .uri("/api/col1/111/col2/{id}", inserted.getId())
      .accept(APPLICATION_JSON)
      .bodyValue(inserted)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(updated);
    assertEquals(path("col1/111/col2/" + inserted.getId()), updated.path());
    assertEquals(Optional.of("newValue"), updated.get("newField"));
  }

  @Test
  public void createAndUpdateDocumentAtLevel3() throws Exception {
    Document doc = new Document();
    doc.setCollection("col1/111/col2/222/col3");
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/111/col2/222/col3")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    inserted.set("newField", "newValue");
    DocumentWithId updated = rest.put()
      .uri("/api/col1/111/col2/222/col3/{id}", inserted.getId())
      .accept(APPLICATION_JSON)
      .bodyValue(inserted)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(updated);
    assertEquals(path("col1/111/col2/222/col3/" + inserted.getId()), updated.path());
    assertEquals(Optional.of("newValue"), updated.get("newField"));
  }

  @Test
  public void createAndUpdateDocumentAtLevel4() throws Exception {
    Document doc = new Document();
    doc.setCollection("col1/111/col2/222/col3");
    doc.set("f1", "val1");

    DocumentWithId inserted = rest.post()
      .uri("/api/col1/111/col2/222/col3/333/col4")
      .accept(APPLICATION_JSON)
      .bodyValue(doc)
      .exchange()
      .expectStatus()
      .isCreated()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    inserted.set("newField", "newValue");
    DocumentWithId updated = rest.put()
      .uri("/api/col1/111/col2/222/col3/333/col4/{id}", inserted.getId())
      .accept(APPLICATION_JSON)
      .bodyValue(inserted)
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(APPLICATION_JSON)
      .expectBody(DocumentWithId.class)
      .returnResult()
      .getResponseBody();

    assertNotNull(updated);
    assertEquals(path("col1/111/col2/222/col3/333/col4/" + inserted.getId()), updated.path());
    assertEquals(Optional.of("newValue"), updated.get("newField"));
  }

}
