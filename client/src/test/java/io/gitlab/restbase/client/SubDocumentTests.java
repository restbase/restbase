package io.gitlab.restbase.client;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;
import static io.gitlab.restbase.test.utils.MockResponseBuilder.response;
import static io.gitlab.restbase.test.utils.TestUrlUtils.url;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import io.gitlab.restbase.DocumentWithId;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

public class SubDocumentTests {

  static final String ID = "id";
  static final String DEFAULT_ID = "123";
  static final String DEFAULT_ID2 = "456";
  static final String COLLECTION = "collection";

  static String API_KEY = "testApiKey";
  private String rootUrl;
  private String apiUrl;

  private RestBaseConfiguration config;
  private Restbase restbase;
  private MockWebServer server;
  private ObjectMapper objectMapper;

  @Before
  public void setup() {
    server = new MockWebServer();
    rootUrl = server.url("/") + "";
    apiUrl = rootUrl + "api";
    config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    objectMapper = restbase.getObjectMapper();
  }

  @After
  public void shutdown() throws Exception {
    server.shutdown();
  }

  @Test
  public void initialize() {
    assertNotNull(restbase);
  }

  @Test(timeout = 5000)
  public void addSubDocument() throws Throwable {
    ImmutableMap<String, Object> fields1 = ImmutableMap.of(ID, DEFAULT_ID, COLLECTION, "collection1", "string1", "test",
        "number1", 123);
    ImmutableMap<String, Object> doc2 = ImmutableMap.of(ID, DEFAULT_ID2, COLLECTION, "collection1/123/collection2",
        "string2", "test2");

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, fields1)
      .enqueue();
    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, doc2)
      .enqueue();

    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test1")
        .with("number1", 123))
      .collection("collection2")
      .add(document().with("string2", "test2"))
      .block();

    assertNotNull(document);
    assertEquals(of("test2"), document.get("string2"));
    assertEquals(empty(), document.get("number1"));

    assertEquals(2, server.getRequestCount());

    RecordedRequest request;

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1"), request.getRequestUrl());
    assertEquals("POST", request.getMethod());

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1/123/collection2"), request.getRequestUrl());
    assertEquals("POST", request.getMethod());
  }

  @Test
  public void updateSubDocument() throws Throwable {
    ImmutableMap<String, Object> fields1 = ImmutableMap.of(ID, DEFAULT_ID, COLLECTION, "collection1", "string1", "test",
        "number1", 123);
    ImmutableMap<String, Object> doc2 = ImmutableMap.of(ID, DEFAULT_ID2, COLLECTION, "collection1/123/collection2",
        "string2", "test2");

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, fields1)
      .enqueue();

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, doc2)
      .enqueue();

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, doc2)
      .enqueue();

    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test1")
        .with("number1", 123))
      .collection("collection2")
      .add(document().with("string2", "test2"))
      .block();

    document.set("newField", "newValue");
    DocumentWithId updated = restbase.update(document)
      .block();

    assertNotNull(updated);
    assertEquals(of("newValue"), document.get("newField"));

    assertEquals(3, server.getRequestCount());

    RecordedRequest request;

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1"), request.getRequestUrl());
    assertEquals("POST", request.getMethod());

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1/123/collection2"), request.getRequestUrl());
    assertEquals("POST", request.getMethod());

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1/123/collection2/456"), request.getRequestUrl());
    assertEquals("PUT", request.getMethod());
  }

  @Test(timeout = 1000, expected = DocumentNotFoundException.class)
  public void deleteDocument() throws Throwable {
    ImmutableMap<String, Object> doc = ImmutableMap.of(ID, DEFAULT_ID, COLLECTION, "collection1", "string1", "test",
        "number1", 123);

    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, doc)
      .enqueue();
    response().on(server)
      .withStatus(HttpStatus.OK)
      .contentType(MediaType.APPLICATION_JSON)
      .body(objectMapper, doc)
      .enqueue();
    response().on(server)
      .withStatus(HttpStatus.NOT_FOUND)
      .enqueue();

    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test")
        .with("number1", 123))
      .block();

    Long id = document.getId();
    restbase.delete(document)
      .block();

    restbase.collection("collection1")
      .document(id)
      .block();

    assertEquals(3, server.getRequestCount());

    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1"), request.getRequestUrl()
      .toString());
    assertEquals("POST", request.getMethod());

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1", DEFAULT_ID), request.getRequestUrl()
      .toString());
    assertEquals("DELETE", request.getMethod());

    request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1", DEFAULT_ID), request.getRequestUrl()
      .toString());
    assertEquals("GET", request.getMethod());
  }

  @Test(timeout = 1000, expected = DocumentNotFoundException.class)
  public void deleteMissingDocument() throws Throwable {
    ImmutableMap<String, Object> doc = ImmutableMap.of(ID, DEFAULT_ID, COLLECTION, "collection1", "string1", "test",
        "number1", 123);
    String docAsString = objectMapper.writeValueAsString(doc);
    DocumentWithId document = objectMapper.readValue(docAsString, DocumentWithId.class);

    response().on(server)
      .withStatus(HttpStatus.NOT_FOUND)
      .enqueue();

    restbase.delete(document)
      .block();

    assertEquals(1, server.getRequestCount());
    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1", DEFAULT_ID), request.getRequestUrl()
      .toString());
    assertEquals("DELETE", request.getMethod());
  }

  @Test(timeout = 1000, expected = DocumentNotFoundException.class)
  public void getMissingDocument() throws Throwable {
    response().on(server)
      .withStatus(HttpStatus.NOT_FOUND)
      .enqueue();

    Long missing = -365L;
    restbase.collection("collection1")
      .document(missing)
      .block();

    assertEquals(1, server.getRequestCount());
    RecordedRequest request = server.takeRequest();
    assertEquals(url(apiUrl, "collection1", "missing"), request.getRequestUrl()
      .toString());
    assertEquals("GET", request.getMethod());
  }

}
