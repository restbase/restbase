package io.gitlab.restbase.client.message;

import static io.gitlab.restbase.DocumentWithId.ROOT;
import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.restbase;
import static io.gitlab.restbase.message.TopicEncoder.changesTopic;
import static io.gitlab.restbase.message.TopicEncoder.childrenTopic;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.message.ChildEventType;
import io.gitlab.restbase.message.DocumentChildEvent;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = SimpleBrokerTestApplication.class)
public class RootDocumentMessageTest {

  static final String API_KEY = "testApiKey";

  private RestBaseConfiguration config;
  private Restbase restbase;

  @Value("${local.server.port}")
  private int port;

  private StompSession stompSession;

  @Before
  public void setup() {
    String rootUrl = "http://localhost:" + port;

    config = config(rootUrl, API_KEY);
    restbase = restbase(config);

    stompSession = restbase.getStompSession();
  }

  @Test(timeout = 5000)
  public void valueChangedAndDisconnected() {
    DocumentWithId root = DocumentWithId.ROOT;

    StepVerifier.create(restbase.changes(root.path()))
      .then(() -> {
        stompSession.send(changesTopic(root), root);
      })
      .expectNext(root)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }

  @Test(timeout = 5000)
  public void addChildAndDisconnect() {
    DocumentChildEvent added = new DocumentChildEvent(ROOT.path(), "collection1", ChildEventType.ADDED);

    StepVerifier.create(restbase.children())
      .then(() -> {
        stompSession.send(childrenTopic(""), added);
      })
      .expectNext(added)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }

  @Test(timeout = 5000)
  public void removeChildAndDisconnect() {
    DocumentChildEvent removed = new DocumentChildEvent(ROOT.path(), "collection1", ChildEventType.REMOVED);

    StepVerifier.create(restbase.children())
      .then(() -> {
        stompSession.send(childrenTopic(""), removed);
      })
      .expectNext(removed)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }
}
