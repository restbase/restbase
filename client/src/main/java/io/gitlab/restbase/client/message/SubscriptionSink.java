package io.gitlab.restbase.client.message;

import org.springframework.messaging.simp.stomp.StompSession.Subscription;

import reactor.core.publisher.FluxSink;

public class SubscriptionSink<T> {

  final Subscription subscription;
  final FluxSink<T> sink;

  public SubscriptionSink(Subscription subscription, FluxSink<T> sink) {
    this.subscription = subscription;
    this.sink = sink;
  }

}
