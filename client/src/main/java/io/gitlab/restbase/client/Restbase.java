package io.gitlab.restbase.client;

import org.springframework.messaging.simp.stomp.StompSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.message.StompClient;
import io.gitlab.restbase.message.ChildEvent;
import io.gitlab.restbase.message.DocumentChildEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Restbase {

  final RestClient restClient;
  final StompClient stompClient;
  final ObjectMapper objectMapper;

  public Restbase(RestBaseConfiguration config) {
    this(new ClientBuilder(config));
  }

  public Restbase(ClientBuilder clientBuilder) {
    this.objectMapper = clientBuilder.getObjectMapper();
    this.stompClient = clientBuilder.getStompClient();
    this.restClient = clientBuilder.getRestClient();
  }

  public CollectionOperations collection(String name) {
    return new CollectionOperations(restClient, stompClient, Path.ROOT, name);
  }

  public static DocumentBuilder document() {
    return new DocumentBuilder();
  }

  public static Restbase restbase(RestBaseConfiguration config) {
    return new Restbase(config);
  }

  public static RestBaseConfiguration config(String url, String apiKey) {
    return new RestBaseConfiguration(url, apiKey);
  }

  public Mono<DocumentWithId> delete(DocumentWithId document) {
    return restClient.delete(document);
  }

  public DocumentOperations update(DocumentWithId document) {
    return restClient.put(document);
  }

  public Mono<Boolean> online() {
    return restClient.healthcheck();
  }

  public RestClient getRestClient() {
    return restClient;
  }

  public Flux<Collection> collections() {
    return restClient.list(DocumentWithId.ROOT);
  }

  public ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  public StompSession getStompSession() {
    return stompClient.session();
  }

  public Flux<DocumentChildEvent> children() {
    return stompClient.subscribeChildren(DocumentWithId.ROOT);
  }

  public Flux<ChildEvent> children(Path path) {
    return stompClient.subscribeChildren(path);
  }

  public Flux<DocumentWithId> changes(Path path) {
    return stompClient.subscribeChanges(path);
  }

  public Flux<DocumentWithId> changes() {
    return stompClient.subscribeChanges(Path.ROOT);
  }

}
