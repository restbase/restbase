package io.gitlab.restbase.client;

public class RestBaseConfiguration {

  public static final String STOMP_ENDPOINT = "/topic";
  public static final String APP_PREFIX = "/app";

  private String url;
  private String socketUrl;
  private String apiKey;

  public RestBaseConfiguration(String url, String apiKey) {
    this.url = url;
    this.socketUrl = url.replaceAll("https://", "wss://")
      .replaceAll("http://", "ws://") + STOMP_ENDPOINT;
    this.apiKey = apiKey;
  }

  public String getUrl() {
    return url;
  }
  
  public String getSocketUrl() {
    return socketUrl;
  }

  public String getApiKey() {
    return apiKey;
  }

}
