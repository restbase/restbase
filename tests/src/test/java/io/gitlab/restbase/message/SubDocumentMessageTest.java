package io.gitlab.restbase.message;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.client.DocumentOperations;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.server.RestbaseServer;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { RestbaseServer.class,
    WebSocketConfiguration.class, WebsocketProperties.class })

public class SubDocumentMessageTest {

  static final String API_KEY = "testApiKey";

  Restbase restbase;
  StompSession stompSession;

  @Value("${local.server.port}")
  private int port;

  private DocumentOperations documentOps;

  private DocumentWithId document;

  @Before
  public void init() {
    String rootUrl = "http://localhost:" + port;
    RestBaseConfiguration config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    stompSession = restbase.getStompSession();

    documentOps = restbase.collection("c1")
      .add(document().with("string1", "test1")
        .with("number1", 123));
    document = documentOps.block();
  }

  @Test(timeout = 10000)
  public void changeValue() {

    StepVerifier.create(restbase.changes(document.path()))
      .then(() -> {
        document.set("newKey", "newValue");
        restbase.update(document)
          .block();
      })
      .expectNext(document)
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 10000)
  public void addCollection() throws Throwable {

    StepVerifier.create(restbase.children(document.path()))
      .then(() -> {
        documentOps.collection("c2")
          .add()
          .block();
      })
      .expectNextMatches(event -> document.path()
        .equals(event.getParent()) && ChildEventType.ADDED == event.getEventType())
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 10000)
  public void deleteCollection() throws Throwable {

    documentOps.collection("c3")
      .add()
      .block();

    StepVerifier.create(restbase.children(document.path()))
      .then(() -> {
        documentOps.collection("c3")
          .delete()
          .block();
      })
      .expectNextMatches(event -> document.path()
        .equals(event.getParent()) && ChildEventType.REMOVED == event.getEventType())
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }

}
