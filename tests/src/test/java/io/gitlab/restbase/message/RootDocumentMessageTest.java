package io.gitlab.restbase.message;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.restbase;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.test.context.junit4.SpringRunner;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.Path;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;
import io.gitlab.restbase.server.RestbaseServer;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { RestbaseServer.class,
    WebSocketConfiguration.class, WebsocketProperties.class })
public class RootDocumentMessageTest {
  static final String API_KEY = "testApiKey";

  Restbase restbase;
  StompSession stompSession;

  @Value("${local.server.port}")
  private int port;

  @Before
  public void init() {
    String rootUrl = "http://localhost:" + port;
    RestBaseConfiguration config = config(rootUrl, API_KEY);
    restbase = restbase(config);
    stompSession = restbase.getStompSession();
  }

  @Test(timeout = 10000)
  public void addCollection() {
    StepVerifier.create(restbase.children())
      .then(() -> {
        restbase.collection("collection1")
          .add()
          .block();
      })
      .expectNextMatches(p -> Path.ROOT.equals(p.getParent()) && ChildEventType.ADDED == p.getEventType())
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

  @Test(timeout = 10000)
  public void deleteCollection() {
    restbase.collection("collection2")
      .add()
      .block();

    StepVerifier.create(restbase.children())
      .then(() -> {
        restbase.collection("collection2")
          .delete()
          .block();
      })
      .expectNextMatches(p -> Path.ROOT.equals(p.getParent()) && ChildEventType.REMOVED == p.getEventType())
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();

  }

  @Test(timeout = 10000)
  public void updateRootDocument() {
    StepVerifier.create(restbase.changes())
      .then(() -> {
        DocumentWithId root = DocumentWithId.ROOT;
        root.set("f", 42);
        restbase.update(root)
          .block();
      })
      .expectNextMatches(p -> {
        System.out.println(p);
        return Path.ROOT.equals(p.path()) && Optional.of(42)
          .equals(p.get("f"));
      })
      .then(() -> stompSession.disconnect())
      .expectComplete()
      .verify();
  }

}
