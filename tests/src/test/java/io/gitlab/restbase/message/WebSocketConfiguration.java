package io.gitlab.restbase.message;

import static io.gitlab.restbase.client.RestBaseConfiguration.APP_PREFIX;
import static io.gitlab.restbase.client.RestBaseConfiguration.STOMP_ENDPOINT;
import static io.gitlab.restbase.message.TopicEncoder.DESTINATION_PREFIX;

import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.google.common.base.Strings;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

  final RabbitProperties rabbit;
  final WebsocketProperties websocket;

  public WebSocketConfiguration(RabbitProperties rabbitProperties, WebsocketProperties websocketProperties) {
    this.rabbit = rabbitProperties;
    this.websocket = websocketProperties;
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry registry) {
    String host = (Strings.isNullOrEmpty(websocket.getHost())) ? rabbit.getHost() : websocket.getHost();
    registry.enableStompBrokerRelay(DESTINATION_PREFIX)
      .setRelayHost(host)
      .setRelayPort(websocket.getPort())
      .setClientLogin(websocket.getUsername())
      .setClientPasscode(websocket.getPassword());
    registry.setApplicationDestinationPrefixes(APP_PREFIX);
  }

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint(STOMP_ENDPOINT)
      .setAllowedOrigins("*")
      .withSockJS();
  }

}
