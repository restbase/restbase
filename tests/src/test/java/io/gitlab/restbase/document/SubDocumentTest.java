package io.gitlab.restbase.document;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.RestbaseServerRule;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;

public class SubDocumentTest {
  static final String ID = "id";

  static final String ROOT_URL = "http://localhost:8080";
  static final String API_KEY = "testApiKey";

  Restbase restbase;

  ObjectMapper objectMapper;

  @ClassRule
  public static final RestbaseServerRule server = new RestbaseServerRule();

  @Before
  public void init() {
    RestBaseConfiguration config = config(ROOT_URL, API_KEY);
    restbase = restbase(config);
    objectMapper = restbase.getObjectMapper();
  }

  @Test
  public void addSubDocument() throws InterruptedException, ExecutionException, JsonProcessingException {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test1")
        .with("number1", 123))
      .collection("collection2")
      .add(document().with("string2", "test2"))
      .block();

    assertNotNull(document);
    assertEquals(Optional.of("test2"), document.get("string2"));
  }

  @Test
  public void updateSubDocument() {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test1")
        .with("number1", 123))
      .collection("collection2")
      .add(document().with("string2", "test2"))
      .block();

    document.set("f", 42);
    DocumentWithId updated = restbase.update(document)
      .block();

    assertNotNull(updated);
    assertEquals(Optional.of(42), document.get("f"));
  }

}
