package io.gitlab.restbase.document;

import static io.gitlab.restbase.client.Restbase.config;
import static io.gitlab.restbase.client.Restbase.document;
import static io.gitlab.restbase.client.Restbase.restbase;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import io.gitlab.restbase.Collection;
import io.gitlab.restbase.DocumentWithId;
import io.gitlab.restbase.RestbaseServerRule;
import io.gitlab.restbase.client.CollectionNotFoundException;
import io.gitlab.restbase.client.DocumentNotFoundException;
import io.gitlab.restbase.client.RestBaseConfiguration;
import io.gitlab.restbase.client.Restbase;

public class RootDocumentTest {
  static final String ID = "id";

  static final String ROOT_URL = "http://localhost:8080";
  static final String API_KEY = "testApiKey";

  Restbase restbase;

  ObjectMapper objectMapper;

  @ClassRule
  public static final RestbaseServerRule server = new RestbaseServerRule();

  @Before
  public void init() {
    RestBaseConfiguration config = config(ROOT_URL, API_KEY);
    restbase = restbase(config);
    objectMapper = restbase.getObjectMapper();
  }

  @Test
  public void addCollection() throws InterruptedException, ExecutionException, JsonProcessingException {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test")
        .with("number1", 123))
      .block();

    assertNotNull(document);
    assertEquals(of("test"), document.get("string1"));
    assertEquals(of(123), document.get("number1"));
  }

  @Test
  public void updateRoot() {
    DocumentWithId root = DocumentWithId.ROOT;
    root.set("f", 42);

    DocumentWithId updated = restbase.update(root)
      .block();
    assertNotNull(updated);
    assertEquals(of(42), updated.get("f"));
  }

  @Test(expected = DocumentNotFoundException.class)
  public void deleteCollection() throws Throwable {
    DocumentWithId document = restbase.collection("collection1")
      .add(document().with("string1", "test")
        .with("number1", 123))
      .block();
    Long id = document.getId();
    DocumentWithId deleted = restbase.delete(document)
      .block();
    assertNotNull(deleted);
    assertEquals(id, deleted.getId());

    restbase.collection("collection1")
      .document(id)
      .block();
  }

  @Test(timeout = 5000)
  public void addAndDeleteCollection() throws Throwable {
    Collection added = restbase.collection("collection-to-del")
      .add()
      .block();
    assertNotNull(added);
    assertEquals("collection-to-del", added.getName());

    restbase.collection("collection-to-del")
      .delete()
      .block();
  }

  @Test(expected = DocumentNotFoundException.class)
  public void deleteMissingCollection() throws Throwable {
    ImmutableMap<String, Object> doc = ImmutableMap.of(ID, -123123, "collection", "missingcollection", "string1",
        "test", "number1", 123);
    String docAsString = objectMapper.writeValueAsString(doc);
    DocumentWithId document = objectMapper.readValue(docAsString, DocumentWithId.class);

    DocumentWithId deleted = restbase.delete(document)
      .block();
    assertNull(deleted);
  }

  @Test(expected = CollectionNotFoundException.class)
  public void getMissingCollection() throws Throwable {
    Collection col = restbase.collection("missing")
      .load()
      .block();
    assertNull(col);
  }

  @Test
  public void getEmptyCollections() throws Throwable {
    List<Collection> collections = restbase.collections()
      .collectList()
      .block();

    assertNotNull(collections);
    assertEquals(0, collections.size());
  }

  @Test
  public void getCollections() throws Throwable {
    Collection c1 = restbase.collection("collection1")
      .add()
      .block();
    Collection c2 = restbase.collection("collection2")
      .add()
      .block();
    List<Collection> collections = restbase.collections()
      .collectList()
      .block();

    assertNotNull(collections);
    assertEquals(2, collections.size());

    assertTrue(collections.contains(c1));
    assertTrue(collections.contains(c2));
  }
}
