#!/bin/sh

set -e
$(which java) -jar /app.jar --spring.profiles.active=prod
