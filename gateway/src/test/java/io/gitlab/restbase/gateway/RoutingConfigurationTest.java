package io.gitlab.restbase.gateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {
    "restbase.api=http://localhost:${wiremock.server.port}" })
@AutoConfigureWireMock(port = 0)
public class RoutingConfigurationTest {

  @Autowired
  private WebTestClient webClient;

  @Test
  public void testRoutes() {
    // Stubs
    stubFor(get(urlEqualTo("/api")).willReturn(aResponse().withBody("")
      .withHeader("Content-Type", new String[] { "application/json" })
      .withHeader("through", new String[] { "restbase" })));

    webClient.get()
      .uri("/api")
      .exchange()
      .expectStatus()
      .isOk()
      .expectHeader()
      .contentType(MediaType.APPLICATION_JSON)
      .expectHeader()
      .exists("through");

  }

}
