package io.gitlab.restbase.gateway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = RestbaseGateway.class)
public class GatewayPropertiesTest {

  @Autowired
  RestbaseProperties properties;
  
  @Test
  public void testConfiguration() {
    assertNotNull(properties);
    assertNotNull(properties.getApi());
    assertEquals("http://server:8080/api", properties.getApi());
  }

}

